/* Script dependencies */
var React = require('react');
var GreenBox = require('./components/green-box/green-box');
var BlueBox = require('./components/blue-box/blue-box');
var GreenCircle = require('./components/green-circle/green-circle');
var BlueCircle = require('./components/blue-circle/blue-circle');

/* Style dependencies */
require('../styles/base.less');

var app = (
    <div>
        <GreenBox/>
        <BlueBox/>
                <GreenCircle/>
        <BlueCircle/>
    </div>
);

window.onload = function() {
    React.render(app, document.getElementById("app"));
};
